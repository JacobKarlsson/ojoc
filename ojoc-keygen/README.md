# OJOC HMAC Keygen Module #

This module generates a HMAC secret using Java JNI, as it is done in
the Android App.

Input is the APK signature hash, and output is the HMAC secret.

## Limitations ##

The file `libhmac.cpp` contains the skeleton of the needed function.

Until the functionality of the original JNI library has been decyphered,
you need to replace the compiled `libhmac.so` file in the `lib` directory with
the library file from the APK which matches your CPU's architecture.

## Usage ##

You may need to link `libstdc++.so.6` to `libstdc++.so` in your `$LD_LIBRARY_PATH`

    make
    <wrong data is output>
    <replace compiled lib/libhmac.so with libhmac.so from APK>
    touch lib/libhmac.so
    make
    <copy output to appropriate field in ../OJOC/Config.py>

To execute the program directly, execute

    java com.ojoc.keygen <Signature Hash>




