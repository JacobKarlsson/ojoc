package com.ojoc;

import com.jodelapp.jodelandroidv3.api.HmacInterceptor;

public class keygen {

    public static void usage (int rv) {
        System.out.println("Usage: keygen <Signature Hash>");
        System.exit(rv);
    }

    public static void main (String[] argv) {
        HmacInterceptor hmac = new HmacInterceptor();
        String secret;
        if (argv.length != 1) {
            for(int i = 0; i < argv.length; i++) {
                System.out.println("argv["+Integer.toString(i)+"]: "+argv[i]);
            }
            usage(-1);
        }
        secret = hmac.generate(argv[0]);
        System.out.println(secret);
        System.exit(0);
    }

}