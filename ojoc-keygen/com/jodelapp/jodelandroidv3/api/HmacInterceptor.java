package com.jodelapp.jodelandroidv3.api;

public class HmacInterceptor {
    static {
        System.loadLibrary("hmac");
    }

    public HmacInterceptor () {
    }

    public native String generate(final String p0);

}