#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import OJOC.Connection
import os
import sys

if __name__ == '__main__':
    conn = OJOC.Connection.Connection()
    id_bytes = os.urandom(32)
    id_bytes_str = ["%02x" % (ord(byte)) for byte in id_bytes]
    conn.device_uid = ''.join(id_bytes_str)
    rv = conn.register()

    print rv

    if (rv is not None):
        sys.exit(0)
    else:
        sys.exit(1)
