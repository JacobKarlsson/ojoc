#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""This is the main executable file for the OJOC GTK GUI"""

import json
import tempfile
from StringIO import StringIO
import array
import os
import shutil
import pprint
import argparse
import sys
import enum
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import GLib
from PIL import Image

import OJOC.Connection
import OJOC.gui

import requests

CAT_API_URL = "http://thecatapi.com/api/images/get"
DBG_DIALOG = False


def timesort(post):
    """Returns post's post ID as an int for sorting (roughly time-dependent)"""
    return int(post['post_id'], 16)


class PostCategory(enum.Enum):
    """Enumerate the two categories 'Posts where I interacted' and
       'All posts'."""
    # This might be necessary, because not all sorting
    # methods might be possible to combine with both categories
    # Also, we need this to distinguish between tabs
    # FIXME really needed?
    MY_POSTS = 1
    ALL_POSTS = 2


class OjocWindow(Gtk.Window):
    """ The main window class """

    def __init__(self, recent_posts=None):
        Gtk.Window.__init__(self, title="OJOC")
        self.set_wmclass("OJOC", "OJOC")
        try:
            self.tempdir = tempfile.mkdtemp()
            print self.tempdir
        except:
            print "Cannot create tempdir"
            return False

        iconpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'images', 'icon.png')

        self.set_icon_from_file(iconpath)

        self.set_border_width(3)
        self.set_default_size(650, 960)
        self.notebook = Gtk.Notebook()
        self.notebook.set_scrollable(True)
        self.add(self.notebook)
        self.notebook_pages_dict = {}
        self.my_posts_notebook_page = None

        # contains all ancestors of currently open posts
        self.ancestors = {}

        # Only generate the Debug Dialog if the global flag is set
        if DBG_DIALOG:
            self.debugDialog = OJOC.gui.DebugWindow(self, self)
        else:
            self.debugDialog = None
        self.my_posts_mode = OJOC.Connection.PostType.ALL
        self.posts_mode = OJOC.Connection.PostType.ALL

        # Set up the GTK header bar for karma and 'new post/reply' buttons
        headerbar = Gtk.HeaderBar()
        headerbar.set_show_close_button(True)
        headerbar.props.title = "OJOC"
        reload_button = Gtk.Button.new_from_icon_name('view-refresh', 1)
        reload_button.connect('clicked', self.reload, None)
        post_button = Gtk.Button.new_from_icon_name('list-add', 1)
        post_button.connect('clicked', self.new_post, None)
        karma_button = Gtk.Button.new_from_icon_name('emblem-favorite', 1)
        karma_button.connect('clicked', self.view_karma, None)
        self.connect('key_press_event', self._key_pressed, None)
        headerbar.pack_end(post_button)
        headerbar.pack_end(reload_button)
        headerbar.pack_start(karma_button)
        self.set_titlebar(headerbar)

        # Set up loading splash screen
        loadpath = os.path.join(self.tempdir, 'loading.jpg')
        try:
            # Download an image from thecatapi.com
            with open(loadpath, 'wb') as handle:
                reply = requests.request('GET', CAT_API_URL, params={'format': 'src', 'type': 'jpg', 'size': 'small'})
                if not reply.ok:
                    print "Could not get image."
                for block in reply.iter_content(1024):
                    handle.write(block)
            try:
                # Display it on a featureless dialog
                self.loading = OJOC.gui.ImageViewer(self,GdkPixbuf.Pixbuf.new_from_file(loadpath),top_text="Loading...",bottom_text="Cat pic by thecatapi.com")
                self.loading.show_all()
            except:
                print "Could not display loading image."
        except:
            print "Could not load loading image."

    def initialize(self, citycc):
        """Set up the connection to Jodel, load the content of the
           default tabs
           citycc  A location in City, CC format as in "Vienna, AT"
           """

        # make a new connection object
        self.connection = OJOC.Connection.Connection(citycc=citycc)
        if self.connection is None:
            print "Could not connect to server"
            self.destroy()
            return None
        self.set_title("OJOC: " + self.connection.get_location_string())

        # construct all the tabs
        self.recent_posts()
        self.my_posts()
        self.my_replies()
        self.my_voted_posts()
        try:
            # Kill the loading dialog if it's there
            self.loading.destroy()
        except AttributeError as e:
            print str(e)
        self.show_all()
        return False

    def reload(self, widget, data):
        """Reloads the data in the currently raised notebook page, which is
           referred to by its numerical index given to it by the Notebook widget.

           widget and data are parameters required for callbacks but are
           ignored"""
        current_notebook_page = self.notebook.get_current_page()
        if current_notebook_page == self.recent_posts_tab_num:
            self.recent_posts()
        elif current_notebook_page == self.my_posts_tab_num:
            self.my_posts()
        elif current_notebook_page == self.my_replies_tab_num:
            self.my_replies()
        elif current_notebook_page == self.my_voted_posts_tab_num:
            self.my_voted_posts()
        else:
            for page_id in self.notebook_pages_dict.keys():
                if self.notebook.page_num(self.notebook_pages_dict[page_id]) == current_notebook_page:
                    self._open_post(self, page_id)
                    return

    def new_post(self, widget, data):
        """ Method to set up a post editing dialog, and forward the
            request to the API when editing is finished.

            data is a parameter required for callbacks but is ignored"""
        parent = None
        color = None
        current_notebook_page = self.notebook.get_current_page()
        # Check if this is a new post or a reply to an opened post.
        # This is done by checking the currently opened notebook page number
        # against a dict holding a cross-reference between post IDs and
        # page numbers
        if current_notebook_page != self.recent_posts_tab_num and current_notebook_page != self.my_posts_tab_num:
            for page_id in self.notebook_pages_dict.keys():
                if self.notebook.page_num(self.notebook_pages_dict[page_id]) == current_notebook_page:
                    parent_post_widget = self.ancestors[page_id]
                    color = parent_post_widget.colorspec
                    parent = page_id
                    break

        # Open a new post editing dialog
        dialog = OJOC.gui.PostEditor(self, color)
        dialog_response = dialog.run()

        if dialog_response == 1:
            # Send the post to the API if user clicked OK
            post = dialog.get_post()
            error_message = post.get('Error')
            if error_message is not None:
                print str(error_message)
            else:
                post_data_dict = self.connection.new_post(post['message'],color=post['color'],image_data=post['image_data'],ancestor=parent,name=post['name'])
                if self.debugDialog is not None:
                    self.debugDialog.update('=== New Post ===\n' + pprint.pformat(post_data_dict) + '\n')

        dialog.destroy()
        self.reload(widget, post_data_dict)

    def view_karma(self, widget, data):
        """ This method opens a dialog which shows the user's current karma.
            The karma value is fetched from the API

           widget and data are parameters required for callbacks but are
           ignored"""
        reply = self.connection.karma()

        if self.debugDialog is not None:
            self.debugDialog.update('=== Karma ===\n' + pprint.pformat(reply) + '\n')
        if reply is not None:
            dialog = Gtk.MessageDialog(self,Gtk.DialogFlags.DESTROY_WITH_PARENT,Gtk.MessageType.INFO,Gtk.ButtonsType.CLOSE,'Karma: ' + str(reply['karma']))
            dialog.run()
            dialog.destroy()

# ----------------------------------------------------------------------
# Methods for updating notebook tabs with posts
# ----------------------------------------------------------------------

    def my_replies(self, post_data_dict=None):
        """ Construct a notebook page containing the posts replied to by
            the user's device_uid

            post_data_dict  Initial data for the tab (do not fetch from API)
        """
        # FIXME refactor: my_replies(), my_voted_posts(), my_posts(),#                 recent_posts(), and _open_post() contain
        #                 a large share of copy-paste code

        # Fetch pots from the API if no data is supplied
        if post_data_dict is None:
            post_data_dict = self.connection.my_replies()

        # Display error dialog if data could not be loaded from the API
        if post_data_dict is False:
            error_dialog = Gtk.MessageDialog(parent=self,text="Error!",secondary_text="Could not get data",message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK)
            error_dialog.run()
            error_dialog.destroy()

        self.post_data_dict = post_data_dict
        if self.debugDialog is not None:
            self.debugDialog.update('=== My Replies ===\n' + pprint.pformat(post_data_dict) + '\n')

        try:
            # Try if we already have a page container for this mode
            page_container_widget = self.my_replies_page_container_widget
        except AttributeError as e:
            # Construct a new one if we don't have one already
            page_container_widget = self.my_replies_page_container_widget = Gtk.ScrolledWindow()
            page_container_widget.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
            page_container_width = page_container_widget.get_min_content_width()
            page_container_widget.set_min_content_width(page_container_width + 10)
            page_container_widget.set_margin_top(5)
            page_container_widget.set_margin_bottom(5)
            page_container_widget.set_margin_left(5)
            page_container_widget.set_margin_right(5)
            self.my_replies_tab_num = self.notebook.append_page(page_container_widget, Gtk.Label('My Replies'))

        page_container_widget.foreach(lambda widget, data: page_container_widget.remove(widget), None)

        notebook_page = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        notebook_page.set_row_spacing(3)
        notebook_page.set_border_width(5)
        self.my_posts_notebook_page = notebook_page

        if post_data_dict:
            for post in post_data_dict['posts']:
                notebook_page.add(OJOC.gui.Post(self.tempdir,post,self,self.connection,cb=self._open_post))

        notebook_page.show_all()
        page_container_widget.add(notebook_page)

    def my_posts(self, post_data_dict=None, mode=None):
        """ Construct a notebook page containing the posts posted by
            the user's device_uid

            post_data_dict  Initial data for the tab (do not fetch from API)
            mode  Data fetching mode (e.g. popular mode) from OJOC.Connection.PostType
        """
        # FIXME refactor: my_replies(), my_voted_posts(), my_posts(),#                 recent_posts(), and _open_post() contain
        #                 a large share of copy-paste code
        if mode is None:
            mode = self.my_posts_mode

        if post_data_dict is None:
            if mode is None or mode == OJOC.Connection.PostType.ALL:
                post_data_dict = self.connection.my_posts()
            elif mode == OJOC.Connection.PostType.POPULAR:
                post_data_dict = self.connection.my_popular_posts()
            elif mode == OJOC.Connection.PostType.DISCUSSED:
                post_data_dict = self.connection.my_discussed_posts()

        if post_data_dict is False:
            message_dialog = Gtk.MessageDialog(parent=self,text="Error!",secondary_text="Could not get data",message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK)
            message_dialog.run()
            message_dialog.destroy()

        self.post_data_dict = post_data_dict
        if self.debugDialog is not None:
            self.debugDialog.update('=== My Posts ===\n' + pprint.pformat(post_data_dict) + '\n')

        try:
            # Try if we already have a page container for this mode
            page_container_widget = self.my_posts_page_container_widget
        except AttributeError as e:
            # Construct a new one if we don't have one already
            page_container_widget = self.my_posts_page_container_widget = Gtk.ScrolledWindow()
            page_container_widget.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
            w = page_container_widget.get_min_content_width()
            page_container_widget.set_min_content_width(w + 10)
            page_container_widget.set_margin_top(5)
            page_container_widget.set_margin_bottom(5)
            page_container_widget.set_margin_left(5)
            page_container_widget.set_margin_right(5)
            menu_event_box = Gtk.EventBox.new()
            lab = Gtk.Label('My Posts')
            lab.show()
            menu_event_box.add(lab)
            self.my_posts_tab_num = self.notebook.append_page(page_container_widget, menu_event_box)

            # Construct the context menu for switching the post type
            # The post_types dict contains the different options for
            # which to generate radio buttons
            # This dict is iterated and the list of radio buttons generated

            all_button = Gtk.RadioMenuItem.new_with_label(None, 'All')
            all_button.connect('activate',self._handle_menu,OJOC.Connection.PostType.ALL,PostCategory.MY_POSTS)
            menugroup = all_button.get_group()

            post_types = {'Popular': OJOC.Connection.PostType.POPULAR,'Discussed': OJOC.Connection.PostType.DISCUSSED}

            menu = Gtk.Menu.new()
            menu.attach(all_button, 0, 3, 0, 1)
            button_index = 1

            for button_name in post_types.keys():
                btn = Gtk.RadioMenuItem.new_with_label(menugroup, button_name)
                btn.connect('activate', self._handle_menu,post_types[button_name], PostCategory.MY_POSTS)
                menu.attach(btn, 0, 3, button_index, button_index + 1)
                button_index += 1

            menu_event_box.connect('button-press-event',self._handle_popup, menu)

        # remove all posts in this page & their container
        page_container_widget.foreach(lambda widget, data: page_container_widget.remove(widget), None)

        notebook_page = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        notebook_page.set_row_spacing(3)
        notebook_page.set_border_width(5)
        self.my_posts_notebook_page = notebook_page

        # diplay new posts
        if post_data_dict:
            for post in post_data_dict['posts']:
                notebook_page.add(OJOC.gui.Post(self.tempdir,post,self,self.connection,cb=self._open_post))

        notebook_page.show_all()
        page_container_widget.add(notebook_page)

    def my_voted_posts(self, post_data_dict=None):
        """ Construct a notebook page containing the posts voten on by
            the user's device_uid

            post_data_dict  Initial data for the tab (do not fetch from API)
        """
        if post_data_dict is None:
            post_data_dict = self.connection.my_voted_posts()

        if not post_data_dict:
            message_dialog = Gtk.MessageDialog(parent=self,text="Error!",secondary_text="Could not get data",message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK)
            message_dialog.run()
            message_dialog.destroy()

        self.post_data_dict = post_data_dict
        if self.debugDialog is not None:
            self.debugDialog.update('=== My Voted Posts ===\n' +
                pprint.pformat(post_data_dict) +
                '\n')

        try:
            page_container_widget = self.my_voted_posts_page_container_widget
        except AttributeError as e:
            self.my_voted_posts_page_container_widget = Gtk.ScrolledWindow()
            page_container_widget = self.my_voted_posts_page_container_widget
            page_container_widget.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
            w = page_container_widget.get_min_content_width()
            page_container_widget.set_min_content_width(w + 10)
            page_container_widget.set_margin_top(5)
            page_container_widget.set_margin_bottom(5)
            page_container_widget.set_margin_left(5)
            page_container_widget.set_margin_right(5)
            self.my_voted_posts_tab_num = self.notebook.append_page(page_container_widget, Gtk.Label('My Voted Posts'))

        page_container_widget.foreach(lambda widget, data: page_container_widget.remove(widget), None)

        notebook_page = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        notebook_page.set_row_spacing(3)
        notebook_page.set_border_width(5)
        self.my_posts_notebook_page = notebook_page

        if post_data_dict:
            for post in post_data_dict['posts']:
                notebook_page.add(OJOC.gui.Post(self.tempdir,post,self,self.connection,cb=self._open_post))

        notebook_page.show_all()
        page_container_widget.add(notebook_page)

    def recent_posts(self, post_data_dict=None, mode=None):
        """ Construct a notebook page containing posts by others
            sorted or filtered by the current mode

            post_data_dict  Initial data for the tab (do not fetch from API)
            mode  Data fetching mode (e.g. popular mode) from OJOC.Connection.PostType
        """
        if mode is None:
            mode = self.posts_mode

        if post_data_dict is None:
            if mode is None or mode == OJOC.Connection.PostType.ALL:
                post_data_dict = self.connection.recent_posts()
            elif mode == OJOC.Connection.PostType.POPULAR:
                post_data_dict = self.connection.popular_posts()
            elif mode == OJOC.Connection.PostType.DISCUSSED:
                post_data_dict = self.connection.discussed_posts()
            elif mode == OJOC.Connection.PostType.COMBO:
                raw_post_data_dict = self.connection.combo_posts()
                post_data_dict = {'posts': sorted(raw_post_data_dict['replied'] +
                        raw_post_data_dict['voted'] +
                        raw_post_data_dict['recent'],key=timesort,reverse=True)}
            elif mode == OJOC.Connection.PostType.COUNTRY:
                post_data_dict = self.connection.country_feed()

        if post_data_dict is False:
            message_dialog = Gtk.MessageDialog(parent=self,text="Error!",secondary_text="Could not get data",message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK)
            message_dialog.run()
            message_dialog.destroy()

        self.post_data_dict = post_data_dict

        if self.debugDialog is not None:
            self.debugDialog.update('=== Posts ===\n' + pprint.pformat(post_data_dict) + '\n')

        try:
            page_container_widget = self.recent_posts_page_container_widget
        except AttributeError as e:
            page_container_widget = self.recent_posts_page_container_widget = Gtk.ScrolledWindow()
            page_container_widget.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
            w = page_container_widget.get_min_content_width()
            page_container_widget.set_min_content_width(w + 10)
            page_container_widget.set_margin_top(5)
            page_container_widget.set_margin_bottom(5)
            page_container_widget.set_margin_left(5)
            page_container_widget.set_margin_right(5)

            menu_event_box = Gtk.EventBox.new()
            lab = Gtk.Label('All Posts')
            lab.show()
            menu_event_box.add(lab)
            self.recent_posts_tab_num = self.notebook.append_page(page_container_widget, menu_event_box)

            # Construct the context menu for switching the post type
            # The post_types dict contains the different options for
            # which to generate radio buttons
            # This dict is iterated and the list of radio buttons generated

            all_button = Gtk.RadioMenuItem.new_with_label(None, 'All')
            all_button.connect('activate',self._handle_menu,OJOC.Connection.PostType.ALL,PostCategory.ALL_POSTS)
            menugroup = all_button.get_group()

            post_types = {'Popular': OJOC.Connection.PostType.POPULAR,'Discussed': OJOC.Connection.PostType.DISCUSSED,'Combo': OJOC.Connection.PostType.COMBO}

            menu = Gtk.Menu.new()
            menu.attach(all_button, 0, 3, 0, 1)
            button_index = 1

            for button_name in post_types.keys():
                btn = Gtk.RadioMenuItem.new_with_label(menugroup, button_name)
                btn.connect('activate', self._handle_menu,post_types[button_name], PostCategory.ALL_POSTS)
                menu.attach(btn, 0, 3, button_index, button_index + 1)
                button_index += 1

            menu_event_box.connect('button-press-event',self._handle_popup, menu)

        page_container_widget.foreach(lambda widget, data: page_container_widget.remove(widget), None)

        notebook_page = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        notebook_page.set_row_spacing(3)
        notebook_page.set_border_width(5)
        self.recent_posts_notebook_page = notebook_page

        if post_data_dict:
            try:
                for post in post_data_dict['posts']:
                    notebook_page.add(OJOC.gui.Post(self.tempdir,post,self,self.connection,cb=self._open_post))
            except:
                print post_data_dict
        notebook_page.show_all()
        page_container_widget.add(notebook_page)

    def _open_post(self, widget, post_id):
        """ Construct a new notebook page for the post with the given
            post ID """

        this_post = post = self.connection.particular_post(post_id)
        if this_post is False:
            # We need this code because it is possible to open an
            # own post and then delete it.
            #
            # Here we check if the reloaded post still exists, and when
            # it does not, display an error message and return to
            # all posts.
            dialog = Gtk.MessageDialog(self,Gtk.DialogFlags.DESTROY_WITH_PARENT,Gtk.MessageType.ERROR,Gtk.ButtonsType.CLOSE,'Post could not be loaded\nReturning to All Posts')
            dialog.run()
            dialog.destroy()
            self.notebook.set_current_page(self.recent_posts_tab_num)
            tab_number = self.notebook_pages_dict.get(post_id)
            if tab_number is not None:
                self._close_tab(widget, tab_number)
            self.reload(None, None)
            return True

        if self.debugDialog is not None:
            self.debugDialog.update('=== Particular Post ===\n' + pprint.pformat(this_post) + '\n')

        # get post & answers
        if this_post is None:
            print "Could not fetch " + post_id
            for post in self.post_data_dict['posts']:
                if post['post_id'] == post_id:
                    this_post = post
                    break
        if this_post is None:
            return False

        old_page = self.notebook_pages_dict.get(this_post['post_id'])
        if old_page is not None:
            old_page_index = self.notebook.page_num(old_page)
            if old_page_index != -1:
                self.notebook.remove_page(old_page_index)

        page_container_widget = Gtk.ScrolledWindow()
        self.notebook_pages_dict[this_post['post_id']] = page_container_widget
        page_container_widget.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        w = page_container_widget.get_min_content_width()
        page_container_widget.set_min_content_width(w + 10)
        notebook_page = Gtk.Grid(orientation=Gtk.Orientation.VERTICAL)
        notebook_page.set_row_spacing(3)
        notebook_page.set_border_width(5)
        page_container_widget.add(notebook_page)
        page_container_widget.set_margin_top(5)
        page_container_widget.set_margin_bottom(5)
        page_container_widget.set_margin_left(5)
        page_container_widget.set_margin_right(5)

        user_handle = post.get('user_handle')

        userlist = None

        if user_handle is not None:
            userlist = [post['user_handle']]

        self.ancestors[this_post['post_id']] = orig = OJOC.gui.Post(self.tempdir, post, self, self.connection)
        notebook_page.add(orig)

        # Check if this post has replies, and list them if yes
        children_posts_list = this_post.get('children')
        if children_posts_list is not None and len(children_posts_list) > 0:
            for reply in children_posts_list:
                # Sometimes, the API supplies us with user numbers to
                # view who posted which answer
                user_handle = reply.get('user_handle')
                user_index = None
                if (user_handle is not None) and (userlist is not None):
                    if user_handle not in userlist:
                        userlist.append(user_handle)
                    user_index = userlist.index(user_handle)
                notebook_page.add(OJOC.gui.Post(self.tempdir,reply,self,self.connection,userno=user_index))

        page_container_widget.show_all()
        head = Gtk.Grid()
        head_label = Gtk.Label(this_post['message'][:10] + '...')
        head_label.modify_bg(Gtk.StateType.NORMAL, orig.color)
        head_close = Gtk.Button.new_from_icon_name('window-close', 1)
        head_close.connect('clicked', self._close_tab, page_container_widget)

        head.attach(head_label, 0, 0, 1, 1)
        head.attach_next_to(head_close, head_label,Gtk.PositionType.RIGHT, 1, 1)
        head.set_tooltip_text(this_post['message'])
        head.show_all()

        self.notebook.set_current_page(self.notebook.append_page(page_container_widget, head))

        print str(self.notebook_pages_dict[this_post['post_id']])
        return True

# ----------------------------------------------------------------------
# Internal callbacks
# ----------------------------------------------------------------------

    def _key_pressed(self, widget, ev, data):
        """ Keypress callback for the main window"""
        # Open the debug dialog if the flag is set
        if ev.keyval == Gdk.KEY_F10 and self.debugDialog is not None:
            self.debugDialog.show()

    def _remove(self, widget, data):
        self.remove(widget)

    def _cleanup(self):
        """
        Method to be executed before destroying this widget.
        This method removes the temporary directory which holds the
        images.
        """
        try:
            shutil.rmtree(self.tempdir)
        except OSError as exc:
            if exc.errno != errno.ENOENT:  # ENOENT - no such file or directory
                raise  # re-raise exception

    def _close_tab(self, widget, notebook_page):
        """ Method to close a given notebook tab. """
        tab_index = self.notebook.page_num(notebook_page)
        if tab_index != -1:
            self.notebook.remove_page(tab_index)
            return True
        else:
            return False

    def _handle_popup(self, widget, event, menu):
        """Display a context menu for selecting the post type"""
        if event.button == 3:
            menu.show_all()
            menu.popup(None, widget, None, None, event.button, event.time)

    def _handle_menu(self, widget, posttype, postcategory):
        """ Click handler for selecting the post type """
        if postcategory == PostCategory.MY_POSTS:
            self.my_posts_mode = posttype
            self.my_posts()
            self.notebook.set_current_page(self.my_posts_tab_num)
        else:
            self.posts_mode = posttype
            self.recent_posts()
            self.notebook.set_current_page(self.recent_posts_tab_num)

# ----------------------------------------------------------------------
# End of class OJOCWindow
# ----------------------------------------------------------------------

if __name__ == '__main__':
    # The main routine

    parser = argparse.ArgumentParser()
    parser.add_argument('-c','--citycc',nargs=1,help='Your location, e.g. Vienna, AT',metavar='CITY, CC')

    print """
    OJOC, Copyright (C) 2016 Christian Fibich
    OJOC comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.
    This is free software, and you are welcome to redistribute it
    under certain conditions; see LICENSE for details.
    """

    try:
        n = parser.parse_args(sys.argv[1:])
        args = vars(n)
    except:
        sys.exit(1)

    loc = args.get('citycc')

    if loc is not None:
        print "Location specified as " + loc[0]
        loc = loc[0]

    win = OjocWindow()
    if win is not None:
        win.connect("delete-event", Gtk.main_quit)
        # This timeout is needed to display the loading
        # splash screen for a minimum duration of 100ms
        GLib.timeout_add(100, win.initialize, loc)
        Gtk.main()
        win._cleanup()
